package neo4j.facades;

import java.util.Arrays;
import java.util.concurrent.CompletionStage;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Workspace;
import neo4j.services.WorkspaceService;

/**
 * Facade for handling all asynchronous CRUD operations regarding Artifacts
 *
 * @author Alexander
 *
 */
public class WorkspaceFacadeAsync
{
    private final Driver driver;
    private final WorkspaceService workspaceService;

    private final LoadingCache<Long, CompletionStage<Workspace>> workspaceCache;

    public WorkspaceFacadeAsync(final Driver driver, final WorkspaceService workspaceService) {
        this.driver = driver;
        this.workspaceService = workspaceService;
        workspaceCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return workspaceService.loadCompleteWorkspaceAsync(id, session);
            }
        });
    }

    /**
     * Saves the given workspace with all its subentities asynchronously
     *
     * @param workspace
     * @return
     */
    public CompletionStage<Workspace> saveCompleteWorkspace(final Workspace workspace) {
        return workspaceService.saveCompleteWorkspaceAsync(workspace, driver.session())
                .thenCompose(ignore -> findCompleteWorkspaceById(workspace.getId()));
    }

    /**
     * Saves the given workspace asynchronously (without subentities)
     *
     * @param workspace
     * @return
     */
    public CompletionStage<Workspace> saveWorkspace(final Workspace workspace) {
        return workspaceService.saveWorkspaceAsync(workspace, driver.session())
                .thenCompose(ignore -> findCompleteWorkspaceById(workspace.getId()));
    }

    /**
     * Deletes all workspaces and subentities for the given ids asynchronously
     *
     * @param ids
     */
    public CompletionStage<Long> deleteCompleteWorkspaces(final Long... ids) {
        return workspaceService.deleteCompleteWorkspacesAsync(Arrays.asList(ids), driver.session())
                .whenCompleteAsync((ignore, ignore2) -> workspaceCache.invalidateAll(Arrays.asList(ids)));
    }

    /**
     * Loads the Workspace asynchronously including all subentities for the
     * given id
     *
     * @param id
     * @return
     */
    public CompletionStage<Workspace> findCompleteWorkspaceById(final Long id) {
        return workspaceCache.get(id);
    }
}
