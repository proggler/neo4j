package neo4j.facades;

import java.util.Arrays;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Workspace;
import neo4j.services.WorkspaceService;

/**
 * Facade for handling all CRUD operations regarding Workspaces
 *
 * @author Alexander
 *
 */
public class WorkspaceFacade
{
    private final Driver driver;
    private final WorkspaceService workspaceService;

    private final LoadingCache<Long, Workspace> workspaceCache;

    public WorkspaceFacade(final Driver driver, final WorkspaceService workspaceService) {
        this.driver = driver;
        this.workspaceService = workspaceService;
        workspaceCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return workspaceService.loadCompleteWorkspace(id, session);
            }
        });
    }

    /**
     * Saves the given workspace with all its subentities
     *
     * @param workspace
     * @return
     */
    public Workspace saveCompleteWorkspace(final Workspace workspace) {
        try (Session session = driver.session())
        {
            workspaceService.saveCompleteWorkspace(workspace, session);
        }
        return findCompleteWorkspaceById(workspace.getId());
    }

    /**
     * Saves the given workspace (without subentities)
     *
     * @param workspace
     * @return
     */
    public Workspace saveWorkspace(final Workspace workspace) {
        try (Session session = driver.session())
        {
            workspaceService.saveWorkspace(workspace, session);
        }
        return findCompleteWorkspaceById(workspace.getId());
    }

    /**
     * Deletes all workspaces and subentities for the given ids
     *
     * @param ids
     */
    public void deleteCompleteWorkspaces(final Long... ids) {
        try (Session session = driver.session())
        {
            workspaceService.deleteCompleteWorkspaces(Arrays.asList(ids), session);
        }
        workspaceCache.invalidateAll(Arrays.asList(ids));
    }

    /**
     * Loads the Workspace including all subentities for the given id
     *
     * @param id
     * @return
     */
    public Workspace findCompleteWorkspaceById(final Long id) {
        return workspaceCache.get(id);
    }
}
