package neo4j.facades;

import java.util.concurrent.CompletionStage;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Artifact;
import neo4j.services.ArtifactService;

/**
 * Facade for handling all asynchronous CRUD operations regarding Artifacts
 *
 * @author Alexander
 *
 */
public class ArtifactFacadeAsync
{
    private final Driver driver;
    private final ArtifactService artifactService;

    private final LoadingCache<Long, CompletionStage<Artifact>> artifactCache;

    public ArtifactFacadeAsync(final Driver driver, final ArtifactService artifactService) {
        this.driver = driver;
        this.artifactService = artifactService;
        artifactCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return artifactService.loadCompleteArtifactAsync(id, session);
            }
        });
    }

    /**
     * Saves the given artifact with all its subentities asynchronously
     *
     * @param artifact
     * @return
     */
    public CompletionStage<Artifact> saveCompleteArtifact(final Artifact artifact) {
        return artifactService.saveCompleteArtifactAsync(artifact, driver.session())
                .thenCompose(ignore -> findCompleteArtifactById(artifact.getId()));
    }

    /**
     * Saves the given artifact asynchronously (without subentities)
     *
     * @param artifact
     * @return
     */
    public CompletionStage<Artifact> saveArtifact(final Artifact artifact) {
        return artifactService.saveArtifactAsync(artifact, driver.session())
                .thenCompose(ignore -> findCompleteArtifactById(artifact.getId()));
    }

    /**
     * Loads the Artifact asynchronously including all subentities for the given
     * id
     *
     * @param id
     * @return
     */
    public CompletionStage<Artifact> findCompleteArtifactById(final Long id) {
        return artifactCache.get(id);
    }
}
