package neo4j.facades;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Artifact;
import neo4j.services.ArtifactService;

/**
 * Facade for handling all CRUD operations regarding Artifacts
 *
 * @author Alexander
 *
 */
public class ArtifactFacade
{
    private final Driver driver;
    private final ArtifactService artifactService;

    private final LoadingCache<Long, Artifact> artifactCache;

    public ArtifactFacade(final Driver driver, final ArtifactService artifactService) {
        this.driver = driver;
        this.artifactService = artifactService;
        artifactCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return artifactService.loadCompleteArtifact(id, session);
            }
        });
    }

    /**
     * Saves the given artifact with all its subentities
     *
     * @param artifact
     * @return
     */
    public Artifact saveCompleteArtifact(final Artifact artifact) {
        try (Session session = driver.session())
        {
            artifactService.saveCompleteArtifact(artifact, session);
            return findCompleteArtifactById(artifact.getId());
        }
    }

    /**
     * Saves the given artifact (without subentities)
     *
     * @param artifact
     * @return
     */
    public Artifact saveArtifact(final Artifact artifact) {
        try (Session session = driver.session())
        {
            artifactService.saveArtifact(artifact, session);
            return findCompleteArtifactById(artifact.getId());
        }
    }

    /**
     * Loads the Artifact including all subentities for the given id
     *
     * @param id
     * @return
     */
    public Artifact findCompleteArtifactById(final Long id) {
        return artifactCache.get(id);
    }
}
