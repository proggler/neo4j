package neo4j.facades;

import java.util.concurrent.CompletionStage;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Delta;
import neo4j.services.DeltaService;

/**
 * Facade for handling all asynchronous CRUD operations regarding Artifacts
 *
 * @author Alexander
 *
 */
public class DeltaFacadeAsync
{
    private final Driver driver;
    private final DeltaService deltaService;

    private final LoadingCache<Long, CompletionStage<Delta>> deltaCache;

    public DeltaFacadeAsync(final Driver driver, final DeltaService deltaService) {
        this.driver = driver;
        this.deltaService = deltaService;
        deltaCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return deltaService.loadDeltaAsync(id, session);
            }
        });
    }

    /**
     * Saves the given delta asynchronously
     *
     * @param delta
     */
    public CompletionStage<Delta> saveDelta(final Delta delta) {
        return deltaService.saveDeltaAsync(delta, driver.session())
                .thenCompose(ignore -> findDeltaById(delta.getId()));
    }

    /**
     * Loads the Delta for the given id asynchronously
     *
     * @param id
     * @return
     */
    public CompletionStage<Delta> findDeltaById(final Long id) {
        return deltaCache.get(id);
    }

    /**
     * Finds the latest Delta of a given property of an artifact with the given
     * id asynchronously
     *
     * @param artifactId
     * @param propertyName
     * @return
     */
    public CompletionStage<Delta> findLatestDeltaByArtifactIdAndPropertyName(final Long artifactId,
            final String propertyName) {
        final CompletionStage<Delta> delta = deltaService.findLatestDeltaByArtifactIdAndPropertyNameAsync(artifactId,
                propertyName, driver.session());
        return delta.thenApply(d -> {
            deltaCache.put(d.getId(), delta);
            return d;
        });
    }
}
