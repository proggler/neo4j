package neo4j.facades;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import neo4j.entities.Delta;
import neo4j.services.DeltaService;

/**
 * Facade for handling all CRUD operations regarding Deltas
 *
 * @author Alexander
 *
 */
public class DeltaFacade
{
    private final Driver driver;
    private final DeltaService deltaService;

    private final LoadingCache<Long, Delta> deltaCache;

    public DeltaFacade(final Driver driver, final DeltaService deltaService) {
        this.driver = driver;
        this.deltaService = deltaService;
        deltaCache = Caffeine.newBuilder().build(id -> {
            try (Session session = driver.session())
            {
                return deltaService.loadDelta(id, session);
            }
        });
    }

    /**
     * Saves the given delta
     *
     * @param delta
     */
    public Delta saveDelta(final Delta delta) {
        try (Session session = driver.session())
        {
            deltaService.saveDelta(delta, session);
        }
        return findDeltaById(delta.getId());
    }

    /**
     * Loads the Delta for the given id
     *
     * @param id
     * @return
     */
    public Delta findDeltaById(final Long id) {
        return deltaCache.get(id);
    }

    /**
     * Finds the latest Delta of a given property of an artifact with the given
     * id
     *
     * @param artifactId
     * @param propertyName
     * @return
     */
    public Delta findLatestDeltaByArtifactIdAndPropertyName(final Long artifactId, final String propertyName) {
        try (Session session = driver.session())
        {
            final Delta delta = deltaService.findLatestDeltaByArtifactIdAndPropertyName(artifactId, propertyName,
                    session);
            deltaCache.put(delta.getId(), delta);
            return delta;
        }
    }
}
