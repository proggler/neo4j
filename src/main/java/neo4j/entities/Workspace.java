package neo4j.entities;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A Workspace containing all the {@link Artifact}s
 *
 * @author Alexander
 *
 */
public class Workspace extends Entity
{
    public static final String LABEL = "Workspace";

    public static final String ID = "id";
    public static final String VERSION = "version";

    public static final String PARENT_RELATION_LABEL = "WORKSPACE";
    public static final String ARTIFACTS_RELATION_LABEL = "ARTIFACT";

    private Long id;
    private Long version;
    private Workspace parent;
    private final Set<Artifact> artifacts = new HashSet<>();

    public Workspace() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public Workspace getParent() {
        return parent;
    }

    public void setParent(final Workspace parent) {
        this.parent = parent;
    }

    public void setArtifacts(final Collection<Artifact> artifacts) {
        this.artifacts.clear();
        if (artifacts != null)
        {
            this.artifacts.addAll(artifacts);
        }
    }

    public Collection<Artifact> getArtifacts() {
        return Collections.unmodifiableCollection(artifacts);
    }

    public void addArtifact(final Artifact artifact) {
        this.artifacts.add(artifact);
    }

    public Map<Long, Artifact> getArtifactsByIds() {
        return this.artifacts.stream().collect(Collectors.toMap(Artifact::getId, a -> a));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(version).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Workspace)
        {
            final Workspace other = (Workspace) obj;
            return new EqualsBuilder().append(id, other.id)
                    .append(version, other.version).isEquals();
        }
        return false;
    }

    @Override
    public String toString() {
        return "{" + id + ", " + version + ", "
                + artifacts.stream().limit(5).map(a -> a.toString()).collect(Collectors.joining(",", "[", "]")) + "}";
    }
}
