package neo4j.entities;

/**
 * An abstract entity
 *
 * @author Alexander
 *
 */
public abstract class Entity
{
    public abstract Long getId();
}
