package neo4j.entities;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * An Artifact in a {@link Workspace}
 *
 * @author Alexander
 *
 */
public class Artifact extends Entity
{
    public static final String LABEL = "Artifact";

    public static final String ID = "id";
    public static final String VERSION = "version";

    public static final String DELTAS_RELATION_LABEL = "DELTA";

    private Long id;
    private Long version;
    private final Set<Delta> deltas = new HashSet<>();

    public Artifact() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public Collection<Delta> getDeltas() {
        return Collections.unmodifiableCollection(deltas);
    }

    public void setDeltas(final Collection<Delta> deltas) {
        this.deltas.clear();
        if (deltas != null)
        {
            this.deltas.addAll(deltas);
        }
    }

    public void addDelta(final Delta delta) {
        this.deltas.add(delta);
    }

    public Map<String, List<Delta>> getDeltasByProperty() {
        return this.deltas.stream().collect(Collectors.groupingBy(Delta::getName));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(version).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Artifact)
        {
            final Artifact other = (Artifact) obj;
            return new EqualsBuilder().append(id, other.id)
                    .append(version, other.version).isEquals();
        }
        return false;
    }

    @Override
    public String toString() {
        return "{" + id + ", " + version + ", "
                + deltas.stream().map(a -> a.toString()).collect(Collectors.joining(",", "[", "]")) + "}";
    }
}
