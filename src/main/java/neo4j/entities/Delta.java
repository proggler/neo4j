package neo4j.entities;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A Delta value of an {@link Artifact}
 *
 * @author Alexander
 *
 */
public class Delta extends Entity
{
    public static final String LABEL = "Delta";

    public static final String ID = "id";
    public static final String VERSION = "version";
    public static final String NAME = "name";
    public static final String DATE = "date";
    public static final String VALUE = "value";

    private Long id;
    private Long version;
    private String name;
    private Date date;
    private Serializable value;

    public Delta() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public Serializable getValue() {
        return value;
    }

    public void setValue(final Serializable value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(version).append(name).append(date).append(value).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Delta)
        {
            final Delta other = (Delta) obj;
            return new EqualsBuilder()
                    .append(id, other.id)
                    .append(version, other.version)
                    .append(name, other.name)
                    .append(date, other.date)
                    .append(value, other.value)
                    .isEquals();
        }
        return false;
    }

    @Override
    public String toString() {
        return "{" + id + ", " + version + ", " + name + ", " + date.getTime() + ", " + value + "}";
    }

}
