package neo4j.repositories.relationships;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;

public class Artifact2DeltaRepository extends AbstractRelationshipRepository<Artifact, Delta>
{
    @Override
    protected String getParentLabel() {
        return Artifact.LABEL;
    }

    @Override
    protected String getChildLabel() {
        return Delta.LABEL;
    }

    @Override
    protected String getRelationLabel() {
        return Artifact.DELTAS_RELATION_LABEL;
    }
}
