package neo4j.repositories.relationships;

import static org.neo4j.driver.v1.Values.parameters;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import org.neo4j.driver.v1.Transaction;

import neo4j.entities.Entity;

/**
 * An abstract relationship repository for basic CRUD operations
 *
 * @author Alexander
 *
 * @param <PARENT>
 *            the generic type of the parent entity
 * @param <CHILD>
 *            the generic type of the child entity
 */
public abstract class AbstractRelationshipRepository<PARENT extends Entity, CHILD extends Entity>
{
    public static final String CREATE_RELATIONSHIP_TEMPLATE = ""
            + "UNWIND $data as d "
            + "MATCH (n:<PARENT_LABEL>{id:d.parent_id}) "
            + "UNWIND d.children_ids as child_id "
            + "MATCH (n2:<CHILD_LABEL>{id:child_id}) "
            + "CREATE (n) -[rel:<RELATION_LABEL>]-> (n2) "
            + "RETURN COUNT(rel);";

    private final String createRelationshipTemplate;

    public AbstractRelationshipRepository() {
        this.createRelationshipTemplate = CREATE_RELATIONSHIP_TEMPLATE
                .replace("<PARENT_LABEL>", getParentLabel())
                .replace("<CHILD_LABEL>", getChildLabel())
                .replace("<RELATION_LABEL>", getRelationLabel());
    }

    /**
     * Creates a relationship between {@code parent} and {@code child}
     *
     * @param parent
     * @param child
     * @param tx
     */
    public void createRelationshipWithEntities(final PARENT parent, final CHILD child, final Transaction tx) {
        final Map<PARENT, Collection<CHILD>> map = new HashMap<>();
        map.put(parent, Collections.singleton(child));
        createRelationshipWithEntities(map, tx);
    }

    /**
     * Creates a relationship between the parent node with id {@code parentId}
     * and the child node with id {@code childId}
     *
     * @param parentId
     * @param childId
     * @param tx
     */
    public void createRelationship(final Long parentId, final Long childId, final Transaction tx) {
        final Map<Long, Collection<Long>> map = new HashMap<>();
        map.put(parentId, Collections.singleton(childId));
        createRelationship(map, tx);
    }

    /**
     * Creates relationships between every parent (keys of
     * {@code relations}-map) and children (values of {@code relations}-map)
     *
     * @param relations
     * @param tx
     */
    public void createRelationshipWithEntities(final Map<PARENT, Collection<CHILD>> relations, final Transaction tx) {
        final Map<Long, Collection<Long>> relationsWithIds = new HashMap<>();
        for (final Entry<PARENT, Collection<CHILD>> entry : relations.entrySet())
        {
            if (((entry.getKey() != null) && (entry.getKey().getId() != null))
                    && (entry.getValue() != null) && !entry.getValue().isEmpty())
            {
                Collection<Long> childIds = relationsWithIds.get(entry.getKey().getId());
                if (childIds == null)
                {
                    childIds = new HashSet<>();
                    relationsWithIds.put(entry.getKey().getId(), childIds);
                }
                for (final CHILD child : entry.getValue())
                {
                    if ((child != null) && (child.getId() != null))
                    {
                        childIds.add(child.getId());
                    }
                }
            }
        }
        createRelationship(relationsWithIds, tx);
    }

    /**
     * Creates relationships between every parent (keys of
     * {@code relations}-map) and children (values of {@code relations}-map)
     * asynchronously
     *
     * @param relations
     * @param tx
     * @return
     */
    public CompletionStage<Long> createRelationshipWithEntitiesAsync(final Map<PARENT, Collection<CHILD>> relations,
            final Transaction tx) {
        final Map<Long, Collection<Long>> relationsWithIds = new HashMap<>();
        for (final Entry<PARENT, Collection<CHILD>> entry : relations.entrySet())
        {
            if (((entry.getKey() != null) && (entry.getKey().getId() != null))
                    && (entry.getValue() != null) && !entry.getValue().isEmpty())
            {
                Collection<Long> childIds = relationsWithIds.get(entry.getKey().getId());
                if (childIds == null)
                {
                    childIds = new HashSet<>();
                    relationsWithIds.put(entry.getKey().getId(), childIds);
                }
                for (final CHILD child : entry.getValue())
                {
                    if ((child != null) && (child.getId() != null))
                    {
                        childIds.add(child.getId());
                    }
                }
            }
        }
        return createRelationshipAsync(relationsWithIds, tx);
    }

    /**
     * Creates relationships between every node with the parentIds (keys of
     * {@code relations}-map) and nodes with the childrenIds (values of
     * {@code relations}-map)
     *
     * @param relations
     * @param tx
     */
    public void createRelationship(final Map<Long, Collection<Long>> relations, final Transaction tx) {
        tx.run(createRelationshipTemplate,
                parameters("data", relations.entrySet().stream()
                        .map(e -> createRelationshipParameters(e.getKey(), e.getValue()))
                        .collect(Collectors.toSet())));
    }

    /**
     * Creates relationships between every node with the parentIds (keys of
     * {@code relations}-map) and nodes with the childrenIds (values of
     * {@code relations}-map) asynchronously
     *
     * @param relations
     * @param tx
     * @return .
     */
    public CompletionStage<Long> createRelationshipAsync(final Map<Long, Collection<Long>> relations,
            final Transaction tx) {
        return tx.runAsync(createRelationshipTemplate,
                parameters("data", relations.entrySet().stream()
                        .map(e -> createRelationshipParameters(e.getKey(), e.getValue()))
                        .collect(Collectors.toSet())))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asLong());
    }

    private Map<String, Object> createRelationshipParameters(final Long parentId, final Collection<Long> childIds) {
        final Map<String, Object> map = new HashMap<>();
        map.put("parent_id", parentId);
        map.put("children_ids", childIds);
        return map;
    }

    protected abstract String getParentLabel();

    protected abstract String getChildLabel();

    protected abstract String getRelationLabel();
}
