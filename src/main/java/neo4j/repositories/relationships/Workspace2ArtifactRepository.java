package neo4j.repositories.relationships;

import neo4j.entities.Artifact;
import neo4j.entities.Workspace;

public class Workspace2ArtifactRepository extends AbstractRelationshipRepository<Workspace, Artifact>
{
    @Override
    protected String getParentLabel() {
        return Workspace.LABEL;
    }

    @Override
    protected String getChildLabel() {
        return Artifact.LABEL;
    }

    @Override
    protected String getRelationLabel() {
        return Workspace.ARTIFACTS_RELATION_LABEL;
    }
}
