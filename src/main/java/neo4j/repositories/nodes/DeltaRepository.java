package neo4j.repositories.nodes;

import static neo4j.entities.Delta.DATE;
import static neo4j.entities.Delta.ID;
import static neo4j.entities.Delta.LABEL;
import static neo4j.entities.Delta.NAME;
import static neo4j.entities.Delta.VALUE;
import static neo4j.entities.Delta.VERSION;
import static org.neo4j.driver.v1.Values.parameters;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletionStage;

import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;
import neo4j.services.transformers.DeltaTransformer;

/**
 * Repository for {@link Delta}s
 *
 * @author Alexander
 *
 */
public class DeltaRepository extends AbstractNodeRepository<Delta>
{
    private static final String LOAD_LATEST_DELTA_BY_ART_ID_TEMPLATE = ""
            + "MATCH (art:<ART_LABEL>{<ID>:$art_id}) -[:<DELTA_REL_LABEL>]-> (delta:<DELTA_LABEL>) "
            + "RETURN delta "
            + "ORDER BY delta.<DATE> DESC "
            + "LIMIT 1";
    private static final String MODIFY_DELTA_VALUE_TEMPLATE = ""
            + "MATCH (delta:<DELTA_LABEL>{<ID>:$id}) "
            + "SET delta.<VALUE> = $value, "
            + "    delta.<DATE> = $date "
            + "RETURN delta";
    private static final String CREATE_DELTA_FOR_ARTIFACT_TEMPLATE = ""
            + "MATCH (art:<ART_LABEL>{<ART_ID>:$art_id}) "
            + "CREATE (art) -[:<DELTA_REL_LABEL>]-> (delta:<DELTA_LABEL><DATA_BINDINGS>) "
            + "RETURN delta";
    private static final String LOAD_DELTA_TEMPLATE = ""
            + "MATCH (delta:<DELTA_LABEL>{<ID>:$id}) "
            + "RETURN delta";
    private static final String FIND_LATEST_DELTA_BY_ARTIFACT_ID_AND_PROPERTY_NAME_TEMPLATE = ""
            + "MATCH (art:<ART_LABEL>{<ART_ID>:$art_id}) -[:<DELTA_REL_LABEL>]-> (delta:<DELTA_LABEL>{<NAME>:$name}) "
            + "RETURN delta "
            + "ORDER BY delta.<DATE> DESC "
            + "LIMIT 1";
    private static final String CREATE_NAME_INDEX_TEMPLATE = ""
            + "CREATE INDEX ON :<DELTA_LABEL>(<NAME>)";

    private final String loadLatestDeltaByArtIdTemplate;
    private final String modifyDeltaValueTemplate;
    private final String createDeltaForArtifactTemplate;
    private final String loadDeltaTemplate;
    private final String findLatestDeltaByArtifactIdAndPropertyNameTemplate;
    private final String createNameIndexTemplate;

    public DeltaRepository(final DeltaTransformer deltaTransformer) {
        super(deltaTransformer);
        this.loadLatestDeltaByArtIdTemplate = LOAD_LATEST_DELTA_BY_ART_ID_TEMPLATE
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ID>", Artifact.ID)
                .replace("<DATE>", Delta.DATE);
        this.modifyDeltaValueTemplate = MODIFY_DELTA_VALUE_TEMPLATE
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<ID>", Delta.ID)
                .replace("<VALUE>", Delta.VALUE)
                .replace("<DATE>", Delta.DATE);
        this.createDeltaForArtifactTemplate = CREATE_DELTA_FOR_ARTIFACT_TEMPLATE
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ART_ID>", Artifact.ID)
                .replace("<DATA_BINDINGS>", createDataBindings("$d."));
        this.loadDeltaTemplate = LOAD_DELTA_TEMPLATE
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<ID>", Delta.ID);
        this.findLatestDeltaByArtifactIdAndPropertyNameTemplate = FIND_LATEST_DELTA_BY_ARTIFACT_ID_AND_PROPERTY_NAME_TEMPLATE
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ART_ID>", Artifact.ID)
                .replace("<NAME>", Delta.NAME)
                .replace("<DATE>", Delta.DATE);
        this.createNameIndexTemplate = CREATE_NAME_INDEX_TEMPLATE
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<NAME>", Delta.NAME);
    }

    @Override
    protected String getNodeLabel() {
        return LABEL;
    }

    @Override
    protected Collection<String> getPropertyNames() {
        return Arrays.asList(ID, VERSION, NAME, DATE, VALUE);
    }

    /**
     * Loads the latest {@link Delta} of an {@link Artifact}
     *
     * @param artifactId
     * @param tx
     * @return
     */
    public Node loadLatestDeltaByArtifactId(final Long artifactId, final Transaction tx) {
        return tx.run(loadLatestDeltaByArtIdTemplate, parameters("art_id", artifactId)).single().get(0).asNode();
    }

    /**
     * Updates the value of a {@link Delta} as well as the date
     *
     * @param deltaId
     * @param value
     * @param tx
     * @return
     */
    public Node modifyDeltaValue(final Long deltaId, final Serializable value, final Transaction tx) {
        return tx
                .run(modifyDeltaValueTemplate,
                        parameters("id", deltaId, "value", value, "date", System.currentTimeMillis()))
                .single().get(0).asNode();
    }

    /**
     * Creates a new {@link Delta} for an {@link Artifact}
     *
     * @param artifactId
     * @param delta
     * @param tx
     * @return
     */
    public Node createDeltaForArtifact(final long artifactId, final Delta delta, final Transaction tx) {
        return tx.run(createDeltaForArtifactTemplate, parameters("art_id", artifactId, "d", transformer.map(delta)))
                .single().get(0).asNode();
    }

    public Node loadDelta(final Long id, final Transaction tx) {
        return tx.run(loadDeltaTemplate, parameters("id", id)).single().get(0).asNode();
    }

    public CompletionStage<Node> loadDeltaAsync(final Long id, final Transaction tx) {
        return tx.runAsync(loadDeltaTemplate, parameters("id", id))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asNode());
    }

    public Node findLatestDeltaByArtifactIdAndPropertyName(final Long artifactId, final String propertyName,
            final Transaction tx) {
        return tx.run(findLatestDeltaByArtifactIdAndPropertyNameTemplate,
                parameters("art_id", artifactId, "name", propertyName)).single().get(0).asNode();
    }

    public CompletionStage<Node> findLatestDeltaByArtifactIdAndPropertyNameAsync(final Long artifactId,
            final String propertyName,
            final Transaction tx) {
        return tx.runAsync(findLatestDeltaByArtifactIdAndPropertyNameTemplate,
                parameters("art_id", artifactId, "name", propertyName))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asNode());
    }

    public void createNameIndex(final Transaction tx) {
        tx.run(createNameIndexTemplate);
    }
}
