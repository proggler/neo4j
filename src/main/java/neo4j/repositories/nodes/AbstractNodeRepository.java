package neo4j.repositories.nodes;

import static org.neo4j.driver.v1.Values.parameters;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import org.neo4j.driver.v1.Transaction;

import neo4j.entities.Entity;
import neo4j.services.transformers.Transformer;

/**
 * An abstract node repository for basic CRUD operations
 *
 * @author Alexander
 *
 * @param <E>
 *            the generic type of the entity
 */
public abstract class AbstractNodeRepository<E extends Entity>
{
    private static final String CREATE_ENTITIES_TEMPLATE = ""
            + "UNWIND $data as d "
            + "CREATE (n:<LABEL><DATA_BINDINGS>) "
            + "RETURN COUNT(n)";

    private static final String LOAD_MAX_ID_TEMPLATE = ""
            + "MATCH (n:<LABEL>) "
            + "RETURN MAX(n.id)";

    private static final String ID_UNIQUE_CONSTRAINT_TEMPLATE = ""
            + "CREATE CONSTRAINT ON (n:<LABEL>) "
            + "ASSERT n.id IS UNIQUE";

    private static final String DELETE_DETACH_NODES_TEMPLATE = ""
            + "MATCH (n:<LABEL>) "
            + "WITH n LIMIT <BATCH_SIZE> "
            + "DETACH DELETE n "
            + "RETURN COUNT(*)";

    protected final Transformer<E> transformer;

    private final String createEntitiesTemplate;
    private final String loadMaxIdTemplate;
    private final String idUniqueConstraintTemplate;
    private final String deleteDetachNodesTemplate;

    public AbstractNodeRepository(final Transformer<E> transformer) {
        this.transformer = transformer;
        this.createEntitiesTemplate = CREATE_ENTITIES_TEMPLATE
                .replace("<LABEL>", getNodeLabel())
                .replace("<DATA_BINDINGS>", createDataBindings("d."));
        this.loadMaxIdTemplate = LOAD_MAX_ID_TEMPLATE.replace("<LABEL>", getNodeLabel());
        this.idUniqueConstraintTemplate = ID_UNIQUE_CONSTRAINT_TEMPLATE.replace("<LABEL>", getNodeLabel());
        this.deleteDetachNodesTemplate = DELETE_DETACH_NODES_TEMPLATE.replace("<LABEL>", getNodeLabel());
    }

    /**
     * Create a node out of an entity
     *
     * @param entity
     * @param tx
     */
    public void createNode(final E entity, final Transaction tx) {
        createNodes(Collections.singleton(entity), tx);
    }

    /**
     * Create a node out of an entity asynchronously
     *
     * @param entity
     * @param tx
     */
    public CompletionStage<Long> createNodeAsync(final E entity, final Transaction tx) {
        return createNodesAsync(Collections.singleton(entity), tx);
    }

    /**
     * Create multiple nodes out of multiple entities
     *
     * @param entities
     * @param tx
     */
    public void createNodes(final Collection<E> entities, final Transaction tx) {
        tx.run(createEntitiesTemplate,
                parameters("data", entities.stream().map(transformer::map).collect(Collectors.toSet())));
    }

    /**
     * Create multiple nodes out of multiple entities asynchronously
     *
     * @param entities
     * @param tx
     */
    public CompletionStage<Long> createNodesAsync(final Collection<E> entities,
            final Transaction tx) {
        return tx.runAsync(createEntitiesTemplate,
                parameters("data", entities.stream().map(transformer::map).collect(Collectors.toSet())))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asLong());
    }

    /**
     * Load the highest found id of a node in the repository
     *
     * @param tx
     * @return
     */
    public Long loadMaxId(final Transaction tx) {
        return tx.run(loadMaxIdTemplate).single().get(0).asLong();
    }

    /**
     * Create a unique constraint for the id of the nodes
     *
     * @param tx
     */
    public void createIdUniqueConstraint(final Transaction tx) {
        tx.run(idUniqueConstraintTemplate);
    }

    /**
     * Delete and detach all of the nodes
     *
     * @param batchSize
     * @param tx
     */
    public void deleteDetachAllNodes(final int batchSize, final Transaction tx) {
        long i = 0;
        do
        {
            System.out.println("DELETING...");
            i = tx.run(deleteDetachNodesTemplate.replace("<BATCH_SIZE>", String.valueOf(batchSize)))
                    .single().get(0).asLong();
            System.out.println("DELETED " + i + " Nodes with LABEL '" + getNodeLabel() + "'");
        } while (i > 0);
    }

    /**
     * Returns the {@link Transformer} corresponding the the entity type
     * {@code <E>}
     *
     * @return
     */
    public Transformer<E> getTransformer() {
        return transformer;
    }

    /**
     * Returns the data bindings in the form of {@code {propA:<prefix>propA,
     * propB:<prefix>propB, ... }}
     *
     * @param prefix
     * @return
     */
    protected String createDataBindings(final String prefix) {
        return getPropertyNames().stream().map(n -> n + ":" + prefix + n).collect(Collectors.joining(",", "{", "}"));
    }

    /**
     * Returns the label of the nodes with the entity type {@code <E>}
     *
     * @return
     */
    protected abstract String getNodeLabel();

    /**
     * Returns all property names of the entity with type {@code <E>}
     *
     * @return
     */
    protected abstract Collection<String> getPropertyNames();
}
