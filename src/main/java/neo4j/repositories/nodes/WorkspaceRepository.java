package neo4j.repositories.nodes;

import static neo4j.entities.Workspace.ID;
import static neo4j.entities.Workspace.LABEL;
import static neo4j.entities.Workspace.VERSION;
import static org.neo4j.driver.v1.Values.parameters;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CompletionStage;

import org.apache.commons.lang3.tuple.Triple;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;
import neo4j.entities.Workspace;
import neo4j.services.transformers.WorkspaceTransformer;

/**
 * Repository for {@link Workspace}s
 *
 * @author Alexander
 *
 */
public class WorkspaceRepository extends AbstractNodeRepository<Workspace>
{
    private static final String LOAD_COMPLETE_WORKSPACE_TEMPLATE = ""
            + "MATCH (ws:<WS_LABEL>{<ID>:$id}) -[art_rel:<ART_REL_LABEL>]-> (art:<ART_LABEL>) -[delta_rel:<DELTA_REL_LABEL>]-> (delta:<DELTA_LABEL>) "
            + "RETURN ws, art, delta";

    private static final String DELETE_DETACH_DELTAS_TEMPLATE = ""
            + "MATCH (:<WS_LABEL>{<ID>:$id}) -[:<ART_REL_LABEL>]-> (:<ART_LABEL>) -[:<DELTA_REL_LABEL>]-> (d:<DELTA_LABEL>) "
            + "WITH d "
            + "LIMIT $batch "
            + "DETACH DELETE d "
            + "RETURN COUNT(d)";

    private static final String DELETE_DETACH_ARTIFACTS_TEMPLATE = ""
            + "MATCH (:<WS_LABEL>{<ID>:$id}) -[:<ART_REL_LABEL>]-> (a:<ART_LABEL>) "
            + "WITH a "
            + "LIMIT $batch "
            + "DETACH DELETE a "
            + "RETURN COUNT(a)";

    private static final String DELETE_DETACH_WORKSPACE_TEMPLATE = ""
            + "MATCH (w:<WS_LABEL>{<ID>:$id}) "
            + "DETACH DELETE a "
            + "RETURN COUNT(a)";

    private final String loadCompleteWorkspaceTemplate;
    private final String deleteDetachDeltasTemplate;
    private final String deleteDetachArtifactsTemplate;
    private final String deleteDetachWorkspaceTemplate;

    public WorkspaceRepository(final WorkspaceTransformer transformer) {
        super(transformer);
        this.loadCompleteWorkspaceTemplate = LOAD_COMPLETE_WORKSPACE_TEMPLATE
                .replace("<WS_LABEL>", Workspace.LABEL)
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<ART_REL_LABEL>", Workspace.ARTIFACTS_RELATION_LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ID>", Workspace.ID);
        this.deleteDetachDeltasTemplate = DELETE_DETACH_DELTAS_TEMPLATE
                .replace("<WS_LABEL>", Workspace.LABEL)
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<ART_REL_LABEL>", Workspace.ARTIFACTS_RELATION_LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ID>", Workspace.ID);
        this.deleteDetachArtifactsTemplate = DELETE_DETACH_ARTIFACTS_TEMPLATE
                .replace("<WS_LABEL>", Workspace.LABEL)
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<ART_REL_LABEL>", Workspace.ARTIFACTS_RELATION_LABEL)
                .replace("<ID>", Workspace.ID);
        this.deleteDetachWorkspaceTemplate = DELETE_DETACH_WORKSPACE_TEMPLATE
                .replace("<WS_LABEL>", Workspace.LABEL)
                .replace("<ID>", Workspace.ID);
    }

    /**
     * Loads a complete {@link Workspace} including its {@link Artifact}s and
     * {@link Delta}s
     *
     * @param id
     * @param tx
     * @return
     */
    public Collection<Triple<Node, Node, Node>> loadCompleteWorkspace(final Long id, final Transaction tx) {
        final StatementResult res = tx.run(loadCompleteWorkspaceTemplate, parameters("id", id));

        final Collection<Triple<Node, Node, Node>> result = new HashSet<>();
        while (res.hasNext())
        {
            final Record record = res.next();
            result.add(Triple.of(record.get(0).asNode(), record.get(1).asNode(), record.get(2).asNode()));
        }
        return result;
    }

    public CompletionStage<List<Triple<Node, Node, Node>>> loadCompleteWorkspaceAsync(final Long id,
            final Transaction tx) {
        return tx.runAsync(loadCompleteWorkspaceTemplate, parameters("id", id)).thenCompose(result -> {
            return result.listAsync(
                    record -> Triple.of(record.get(0).asNode(), record.get(1).asNode(), record.get(2).asNode()));
        });
    }

    @Override
    protected String getNodeLabel() {
        return LABEL;
    }

    @Override
    protected Collection<String> getPropertyNames() {
        return Arrays.asList(ID, VERSION);
    }

    public long deleteDetachDeltas(final Long id, final long batchSize, final Transaction tx) {
        return tx.run(deleteDetachDeltasTemplate, parameters("id", id, "batch", batchSize)).single().get(0).asLong();
    }

    public long deleteDetachArtifacts(final Long id, final long batchSize, final Transaction tx) {
        return tx.run(deleteDetachArtifactsTemplate, parameters("id", id, "batch", batchSize)).single().get(0).asLong();
    }

    public long deleteDetachWorkspace(final Long id, final Transaction tx) {
        return tx.run(deleteDetachWorkspaceTemplate, parameters("id", id)).single().get(0).asLong();
    }

    public CompletionStage<Long> deleteDetachDeltasAsync(final Long id, final Long batchSize, final Transaction tx) {
        return tx.runAsync(deleteDetachDeltasTemplate, parameters("id", id, "batch", batchSize))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asLong());
    }

    public CompletionStage<Long> deleteDetachArtifactsAsync(final Long id, final Long batchSize, final Transaction tx) {
        return tx.runAsync(deleteDetachArtifactsTemplate, parameters("id", id, "batch", batchSize))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asLong());
    }

    public CompletionStage<Long> deleteDetachWorkspaceAsync(final Long id, final Transaction tx) {
        return tx.runAsync(deleteDetachWorkspaceTemplate, parameters("id", id))
                .thenCompose(result -> result.singleAsync())
                .thenApply(record -> record.get(0).asLong());
    }
}
