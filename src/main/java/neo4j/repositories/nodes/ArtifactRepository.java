package neo4j.repositories.nodes;

import static neo4j.entities.Artifact.ID;
import static neo4j.entities.Artifact.LABEL;
import static neo4j.entities.Artifact.VERSION;
import static org.neo4j.driver.v1.Values.parameters;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CompletionStage;

import org.apache.commons.lang3.tuple.Pair;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;
import neo4j.entities.Workspace;
import neo4j.services.transformers.ArtifactTransformer;

/**
 * Repository for {@link Artifact}s
 *
 * @author Alexander
 *
 */
public class ArtifactRepository extends AbstractNodeRepository<Artifact>
{
    private static final String LOAD_COMPLETE_ARTIFACT_TEMPLATE = ""
            + "MATCH (art:<ART_LABEL>{<ID>:$id}) -[delta_rel:<DELTA_REL_LABEL>]-> (delta:<DELTA_LABEL>) "
            + "RETURN art, delta";

    private final String loadCompleteArtifactTemplate;

    public ArtifactRepository(final ArtifactTransformer artifactTransformer) {
        super(artifactTransformer);
        this.loadCompleteArtifactTemplate = LOAD_COMPLETE_ARTIFACT_TEMPLATE
                .replace("<ART_LABEL>", Artifact.LABEL)
                .replace("<DELTA_LABEL>", Delta.LABEL)
                .replace("<DELTA_REL_LABEL>", Artifact.DELTAS_RELATION_LABEL)
                .replace("<ID>", Artifact.ID);
    }

    @Override
    protected String getNodeLabel() {
        return LABEL;
    }

    @Override
    protected Collection<String> getPropertyNames() {
        return Arrays.asList(ID, VERSION);
    }

    /**
     * Loads a complete {@link Workspace} including its {@link Artifact}s and
     * {@link Delta}s
     *
     * @param id
     * @param tx
     * @return
     */
    public Collection<Pair<Node, Node>> loadCompleteArtifact(final Long id, final Transaction tx) {
        final StatementResult res = tx.run(loadCompleteArtifactTemplate, parameters("id", id));

        final Collection<Pair<Node, Node>> result = new HashSet<>();
        while (res.hasNext())
        {
            final Record record = res.next();
            result.add(Pair.of(record.get(0).asNode(), record.get(1).asNode()));
        }
        return result;
    }

    public CompletionStage<List<Pair<Node, Node>>> loadCompleteArtifactAsync(final Long id, final Transaction tx) {
        return tx.runAsync(loadCompleteArtifactTemplate, parameters("id", id)).thenCompose(result -> {
            return result.listAsync(record -> Pair.of(record.get(0).asNode(), record.get(1).asNode()));
        });
    }
}
