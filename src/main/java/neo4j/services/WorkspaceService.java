package neo4j.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Triple;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;
import neo4j.entities.Workspace;
import neo4j.repositories.nodes.ArtifactRepository;
import neo4j.repositories.nodes.DeltaRepository;
import neo4j.repositories.nodes.WorkspaceRepository;
import neo4j.repositories.relationships.Artifact2DeltaRepository;
import neo4j.repositories.relationships.Workspace2ArtifactRepository;
import neo4j.services.transformers.Transformer;

/**
 * Service for database operations on {@link Workspace}s
 *
 * @author Alexander
 *
 */
public class WorkspaceService
{
    private final WorkspaceRepository workspaceRepository;
    private final ArtifactRepository artifactRepository;
    private final DeltaRepository deltaRepository;

    private final Workspace2ArtifactRepository workspace2ArtifactRepository;
    private final Artifact2DeltaRepository artifact2DeltaRepository;

    private final Transformer<Workspace> workspaceTransformer;
    private final Transformer<Artifact> artifactTransformer;
    private final Transformer<Delta> deltaTransformer;

    public WorkspaceService(final WorkspaceRepository workspaceRepository, final ArtifactRepository artifactRepository,
            final DeltaRepository deltaRepository, final Workspace2ArtifactRepository workspace2ArtifactRepository,
            final Artifact2DeltaRepository artifact2DeltaRepository) {
        this.workspaceRepository = workspaceRepository;
        this.artifactRepository = artifactRepository;
        this.deltaRepository = deltaRepository;
        this.workspace2ArtifactRepository = workspace2ArtifactRepository;
        this.artifact2DeltaRepository = artifact2DeltaRepository;
        this.workspaceTransformer = workspaceRepository.getTransformer();
        this.artifactTransformer = artifactRepository.getTransformer();
        this.deltaTransformer = deltaRepository.getTransformer();
    }

    public void saveCompleteWorkspace(final Workspace workspace, final Session session) {
        System.out.println("Start saving a Workspace...");
        session.writeTransaction(tx -> {
            // Calculate needed data
            long start = System.currentTimeMillis();
            final Collection<Delta> allDeltas = workspace.getArtifacts().stream()
                    .flatMap(a -> a.getDeltas().stream())
                    .collect(Collectors.toSet());
            final Map<Workspace, Collection<Artifact>> w2aMap = Stream.of(workspace)
                    .collect(Collectors.toMap(ws -> ws, ws -> ws.getArtifacts()));
            final Map<Artifact, Collection<Delta>> a2dMap = workspace.getArtifacts().stream()
                    .collect(Collectors.toMap(art -> art, art -> art.getDeltas()));
            System.out.println(start - (start = System.currentTimeMillis()));
            // Save all nodes
            workspaceRepository.createNode(workspace, tx);
            artifactRepository.createNodes(workspace.getArtifacts(), tx);
            deltaRepository.createNodes(allDeltas, tx);
            System.out.println(start - (start = System.currentTimeMillis()));
            // Save all relationships
            workspace2ArtifactRepository.createRelationshipWithEntities(w2aMap, tx);
            artifact2DeltaRepository.createRelationshipWithEntities(a2dMap, tx);
            System.out.println(start - (start = System.currentTimeMillis()));
            return null;
        });
        System.out.println("Workspace saved!");
    }

    public CompletionStage<Void> saveCompleteWorkspaceAsync(final Workspace workspace, final Session session) {
        return session.writeTransactionAsync(tx -> {
            final Collection<Delta> allDeltas = workspace.getArtifacts().stream()
                    .flatMap(a -> a.getDeltas().stream())
                    .collect(Collectors.toSet());
            final Map<Workspace, Collection<Artifact>> w2aMap = Stream.of(workspace)
                    .collect(Collectors.toMap(ws -> ws, ws -> ws.getArtifacts()));
            final Map<Artifact, Collection<Delta>> a2dMap = workspace.getArtifacts().stream()
                    .collect(Collectors.toMap(art -> art, art -> art.getDeltas()));
            final long start = System.currentTimeMillis();
            return workspaceRepository.createNodeAsync(workspace, tx)
                    .thenCompose(cnt -> {
                        System.out.println(cnt + " Workspaces created!");
                        return artifactRepository.createNodesAsync(workspace.getArtifacts(), tx);
                    }).thenCompose(cnt -> {
                        System.out.println(cnt + " Artifacts created!");
                        return deltaRepository.createNodesAsync(allDeltas, tx);
                    }).thenCompose(cnt -> {
                        System.out.println(cnt + " Deltas created!");
                        return workspace2ArtifactRepository.createRelationshipWithEntitiesAsync(w2aMap, tx);
                    }).thenCompose(cnt -> {
                        System.out.println(cnt + " Workspace2Artifact Relations created!");
                        return artifact2DeltaRepository.createRelationshipWithEntitiesAsync(a2dMap, tx);
                    }).thenApply(cnt -> {
                        System.out.println(cnt + " Artifact2Delta Relations created!");
                        return System.currentTimeMillis() - start;
                    });
        }).thenCompose(timeTaken -> {
            System.out.println("Time taken: " + timeTaken);
            return session.closeAsync();
        });
    }

    public CompletionStage<Long> saveWorkspaceAsync(final Workspace workspace, final Session session) {
        return session.writeTransactionAsync(tx -> {
            return workspaceRepository.createNodeAsync(workspace, tx);
        });
    }

    public void saveWorkspace(final Workspace workspace, final Session session) {
        session.writeTransaction(tx -> {
            workspaceRepository.createNode(workspace, tx);
            return null;
        });
    }

    public void deleteAllWorkspaces(final Session session) {
        session.writeTransaction(tx -> {
            deltaRepository.deleteDetachAllNodes(10000, tx);
            artifactRepository.deleteDetachAllNodes(5000, tx);
            workspaceRepository.deleteDetachAllNodes(1000, tx);
            return null;
        });
    }

    public CompletionStage<Workspace> loadCompleteWorkspaceAsync(final Long id, final Session session) {
        return session.readTransactionAsync(tx -> {
            return workspaceRepository.loadCompleteWorkspaceAsync(id, tx)
                    .thenApply(nodesList -> mapCompleteWorkspace(nodesList, id));
        }).thenApply(ws -> {
            session.closeAsync();
            return ws;
        });
    }

    public Workspace loadCompleteWorkspace(final Long id, final Session session) {
        return session
                .readTransaction(tx -> mapCompleteWorkspace(workspaceRepository.loadCompleteWorkspace(id, tx), id));
    }

    public void saveCompleteWorkspaces(final Collection<Workspace> workspaces, final Session session) {
        workspaces.stream().forEach(ws -> saveCompleteWorkspace(ws, session));
    }

    public CompletionStage<Long> deleteCompleteWorkspacesAsync(final Collection<Long> ids, final Session session) {
        return session.writeTransactionAsync(tx -> {
            return deleteAllDeltas(ids.iterator(), 0l, tx)
                    .thenCompose(deleted -> {
                        System.out.println(deleted + " Deltas deleted!");
                        return deleteAllArtifacts(ids.iterator(), 0l, tx);
                    })
                    .thenCompose(deleted -> {
                        System.out.println(deleted + " Artifacts deleted!");
                        return deleteAllWorkspaces(ids.iterator(), 0l, tx);
                    });
        });
    }

    public void deleteCompleteWorkspaces(final Collection<Long> ids, final Session session) {
        session.writeTransaction(tx -> {
            deleteAllDeltas(ids, tx);
            deleteAllArtifacts(ids, tx);
            deleteAllWorkspaces(ids, tx);
            return null;
        });
    }

    private void deleteAllDeltas(final Collection<Long> ids, final Transaction tx) {
        for (final Long id : ids)
        {
            long i;
            do
            {
                i = workspaceRepository.deleteDetachDeltas(id, 10000L, tx);
            } while (i > 0);
        }
    }

    private void deleteAllArtifacts(final Collection<Long> ids, final Transaction tx) {
        for (final Long id : ids)
        {
            long i;
            do
            {
                i = workspaceRepository.deleteDetachArtifacts(id, 10000L, tx);
            } while (i > 0);
        }
    }

    private void deleteAllWorkspaces(final Collection<Long> ids, final Transaction tx) {
        for (final Long id : ids)
        {
            long i;
            do
            {
                i = workspaceRepository.deleteDetachWorkspace(id, tx);
            } while (i > 0);
        }
    }

    private CompletionStage<Long> deleteAllDeltas(final Iterator<Long> idIterator, final long alreadyDeleted,
            final Transaction tx) {

        if (idIterator.hasNext())
        {
            return deleteAllDeltas(idIterator.next(), 0, tx)
                    .thenCompose(deleted -> deleteAllDeltas(idIterator, deleted + alreadyDeleted, tx));
        } else
        {
            return CompletableFuture.completedFuture(alreadyDeleted);
        }
    }

    private CompletionStage<Long> deleteAllDeltas(final Long id, final long alreadyDeleted, final Transaction tx) {
        return workspaceRepository.deleteDetachDeltasAsync(id, 10000l, tx)
                .thenCompose(deleted -> deleted > 0
                        ? deleteAllDeltas(id, alreadyDeleted + deleted, tx)
                        : CompletableFuture.completedFuture(alreadyDeleted));
    }

    private CompletionStage<Long> deleteAllArtifacts(final Iterator<Long> idIterator, final long alreadyDeleted,
            final Transaction tx) {

        if (idIterator.hasNext())
        {
            return deleteAllArtifacts(idIterator.next(), 0, tx)
                    .thenCompose(deleted -> deleteAllArtifacts(idIterator, deleted + alreadyDeleted, tx));
        } else
        {
            return CompletableFuture.completedFuture(alreadyDeleted);
        }
    }

    private CompletionStage<Long> deleteAllArtifacts(final Long id, final long alreadyDeleted, final Transaction tx) {
        return workspaceRepository.deleteDetachArtifactsAsync(id, 10000l, tx)
                .thenCompose(deleted -> deleted > 0
                        ? deleteAllArtifacts(id, alreadyDeleted + deleted, tx)
                        : CompletableFuture.completedFuture(alreadyDeleted));
    }

    private CompletionStage<Long> deleteAllWorkspaces(final Iterator<Long> idIterator, final long alreadyDeleted,
            final Transaction tx) {

        if (idIterator.hasNext())
        {
            return workspaceRepository.deleteDetachWorkspaceAsync(idIterator.next(), tx)
                    .thenCompose(deleted -> deleteAllWorkspaces(idIterator, deleted + alreadyDeleted, tx));
        } else
        {
            return CompletableFuture.completedFuture(alreadyDeleted);
        }
    }

    private Workspace mapCompleteWorkspace(final Collection<Triple<Node, Node, Node>> nodesList, final Long id) {
        final Map<Long, Workspace> workspaces = new HashMap<>();
        final Map<Long, Artifact> artifacts = new HashMap<>();
        final Map<Long, Delta> deltas = new HashMap<>();
        for (final Triple<Node, Node, Node> nodes : nodesList)
        {
            final Node wsNode = nodes.getLeft();
            final Node artNode = nodes.getMiddle();
            final Node deltaNode = nodes.getRight();

            final Long wsId = wsNode.get("id").asLong();
            final Long artId = artNode.get("id").asLong();
            final Long deltaId = deltaNode.get("id").asLong();

            Workspace workspace = workspaces.get(wsId);
            Artifact artifact = artifacts.get(artId);
            Delta delta = deltas.get(deltaId);

            if (workspace == null)
            {
                workspace = workspaceTransformer.map(wsNode.asMap());
                workspaces.put(wsId, workspace);
            }
            if (artifact == null)
            {
                artifact = artifactTransformer.map(artNode.asMap());
                artifacts.put(artId, artifact);
            }
            if (delta == null)
            {
                delta = deltaTransformer.map(deltaNode.asMap());
                deltas.put(deltaId, delta);
            }

            workspace.addArtifact(artifact);
            artifact.addDelta(delta);
        }
        return workspaces.get(id);
    }
}
