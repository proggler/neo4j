package neo4j.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Artifact;
import neo4j.entities.Delta;
import neo4j.repositories.nodes.ArtifactRepository;
import neo4j.repositories.nodes.DeltaRepository;
import neo4j.repositories.relationships.Artifact2DeltaRepository;
import neo4j.services.transformers.Transformer;

/**
 * Service for database operations on {@link Artifact}s
 *
 * @author Alexander
 *
 */
public class ArtifactService
{
    private final ArtifactRepository artifactRepository;
    private final DeltaRepository deltaRepository;

    private final Artifact2DeltaRepository artifact2DeltaRepository;

    private final Transformer<Artifact> artifactTransformer;
    private final Transformer<Delta> deltaTransformer;

    public ArtifactService(final ArtifactRepository artifactRepository, final DeltaRepository deltaRepository,
            final Artifact2DeltaRepository artifact2DeltaRepository) {
        this.artifactRepository = artifactRepository;
        this.deltaRepository = deltaRepository;
        this.artifact2DeltaRepository = artifact2DeltaRepository;
        this.artifactTransformer = artifactRepository.getTransformer();
        this.deltaTransformer = deltaRepository.getTransformer();
    }

    public void saveCompleteArtifact(final Artifact artifact, final Session session) {
        System.out.println("Start saving a Artifact...");
        session.writeTransaction(tx -> {
            // Calculate needed data
            long start = System.currentTimeMillis();
            final Map<Artifact, Collection<Delta>> a2dMap = Stream.of(artifact)
                    .collect(Collectors.toMap(art -> art, art -> art.getDeltas()));
            System.out.println(start - (start = System.currentTimeMillis()));
            // Save all nodes
            artifactRepository.createNode(artifact, tx);
            deltaRepository.createNodes(artifact.getDeltas(), tx);
            System.out.println(start - (start = System.currentTimeMillis()));
            // Save all relationships
            artifact2DeltaRepository.createRelationshipWithEntities(a2dMap, tx);
            System.out.println(start - (start = System.currentTimeMillis()));
            return null;
        });
        System.out.println("Artifact saved!");
    }

    public CompletionStage<Void> saveCompleteArtifactAsync(final Artifact artifact, final Session session) {
        return session.writeTransactionAsync(tx -> {
            final Map<Artifact, Collection<Delta>> a2dMap = Stream.of(artifact)
                    .collect(Collectors.toMap(art -> art, art -> art.getDeltas()));
            final long start = System.currentTimeMillis();
            return artifactRepository.createNodeAsync(artifact, tx)
                    .thenCompose(cnt -> {
                        System.out.println(cnt + " Artifacts created!");
                        return deltaRepository.createNodesAsync(artifact.getDeltas(), tx);
                    }).thenCompose(cnt -> {
                        System.out.println(cnt + " Deltas created!");
                        return artifact2DeltaRepository.createRelationshipWithEntitiesAsync(a2dMap, tx);
                    }).thenApply(cnt -> {
                        System.out.println(cnt + " Artifact2Delta Relations created!");
                        return System.currentTimeMillis() - start;
                    });
        }).thenCompose(timeTaken -> {
            System.out.println("Time taken: " + timeTaken);
            return session.closeAsync();
        });
    }

    public void saveArtifact(final Artifact artifact, final Session session) {
        session.writeTransaction(tx -> {
            artifactRepository.createNode(artifact, tx);
            return null;
        });
    }

    public CompletionStage<Void> saveArtifactAsync(final Artifact artifact, final Session session) {
        return session.writeTransactionAsync(tx -> {
            return artifactRepository.createNodeAsync(artifact, tx);
        }).thenCompose(ignore -> session.closeAsync());
    }

    public Artifact loadCompleteArtifact(final Long id, final Session session) {
        return session.readTransaction(tx -> {
            final Map<Long, Artifact> artifacts = new HashMap<>();
            final Map<Long, Delta> deltas = new HashMap<>();
            for (final Pair<Node, Node> nodes : artifactRepository.loadCompleteArtifact(id, tx))
            {
                final Node artNode = nodes.getLeft();
                final Node deltaNode = nodes.getRight();

                final Long artId = artNode.get("id").asLong();
                final Long deltaId = deltaNode.get("id").asLong();

                Artifact artifact = artifacts.get(artId);
                Delta delta = deltas.get(deltaId);

                if (artifact == null)
                {
                    artifact = artifactTransformer.map(artNode.asMap());
                    artifacts.put(artId, artifact);
                }
                if (delta == null)
                {
                    delta = deltaTransformer.map(deltaNode.asMap());
                    deltas.put(deltaId, delta);
                }

                artifact.addDelta(delta);
            }
            return artifacts.get(id);
        });
    }

    public CompletionStage<Artifact> loadCompleteArtifactAsync(final Long id, final Session session) {
        return session.readTransactionAsync(tx -> {
            return artifactRepository.loadCompleteArtifactAsync(id, tx)
                    .thenApply(nodesList -> mapCompleteArtifact(nodesList, id));
        }).thenApply(art -> {
            session.closeAsync();
            return art;
        });
    }

    public void saveCompleteArtifacts(final Collection<Artifact> workspaces, final Session session) {
        workspaces.stream().forEach(ws -> saveCompleteArtifact(ws, session));
    }

    private Artifact mapCompleteArtifact(final Collection<Pair<Node, Node>> nodesList, final Long id) {
        final Map<Long, Artifact> artifacts = new HashMap<>();
        final Map<Long, Delta> deltas = new HashMap<>();
        for (final Pair<Node, Node> nodes : nodesList)
        {
            final Node artNode = nodes.getLeft();
            final Node deltaNode = nodes.getRight();

            final Long artId = artNode.get("id").asLong();
            final Long deltaId = deltaNode.get("id").asLong();

            Artifact artifact = artifacts.get(artId);
            Delta delta = deltas.get(deltaId);

            if (artifact == null)
            {
                artifact = artifactTransformer.map(artNode.asMap());
                artifacts.put(artId, artifact);
            }
            if (delta == null)
            {
                delta = deltaTransformer.map(deltaNode.asMap());
                deltas.put(deltaId, delta);
            }

            artifact.addDelta(delta);
        }
        return artifacts.get(id);
    }
}
