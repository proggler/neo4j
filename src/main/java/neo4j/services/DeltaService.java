package neo4j.services;

import java.io.Serializable;
import java.util.concurrent.CompletionStage;

import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.types.Node;

import neo4j.entities.Delta;
import neo4j.repositories.nodes.DeltaRepository;
import neo4j.services.transformers.Transformer;

/**
 * Service for database operations on {@link Delta}s
 *
 * @author Alexander
 *
 */
public class DeltaService
{
    private final DeltaRepository deltaRepository;

    private final Transformer<Delta> deltaTransformer;

    public DeltaService(final DeltaRepository deltaRepository) {
        this.deltaRepository = deltaRepository;
        this.deltaTransformer = deltaRepository.getTransformer();
    }

    public Delta loadLatestDeltaByArtifactId(final Long artifactId, final Session session) {
        return session.readTransaction(tx -> {
            final Node delta = deltaRepository.loadLatestDeltaByArtifactId(artifactId, tx);
            return deltaTransformer.map(delta.asMap());
        });
    }

    public Delta modifyDeltaValue(final Long deltaId, final Serializable value, final Session session) {
        return session.writeTransaction(tx -> {
            final Node delta = deltaRepository.modifyDeltaValue(deltaId, value, tx);
            return deltaTransformer.map(delta.asMap());
        });
    }

    public Delta createDeltaForArtifact(final long artifactId, final Delta delta, final Session session) {
        return session.writeTransaction(tx -> {
            final Node newDelta = deltaRepository.createDeltaForArtifact(artifactId, delta, tx);
            return deltaTransformer.map(newDelta.asMap());
        });
    }

    public CompletionStage<Long> saveDeltaAsync(final Delta delta, final Session session) {
        return session.writeTransactionAsync(tx -> {
            return deltaRepository.createNodeAsync(delta, tx);
        });
    }

    public void saveDelta(final Delta delta, final Session session) {
        session.writeTransaction(tx -> {
            deltaRepository.createNode(delta, tx);
            return null;
        });
    }

    public Delta loadDelta(final Long id, final Session session) {
        return session.readTransaction(tx -> {
            final Node delta = deltaRepository.loadDelta(id, tx);
            return deltaTransformer.map(delta.asMap());
        });
    }

    public CompletionStage<Delta> loadDeltaAsync(final Long id, final Session session) {
        return session.readTransactionAsync(tx -> {
            return deltaRepository.loadDeltaAsync(id, tx).thenApply(delta -> deltaTransformer.map(delta.asMap()));
        });
    }

    public Delta findLatestDeltaByArtifactIdAndPropertyName(final Long artifactId, final String propertyName,
            final Session session) {
        return session.readTransaction(tx -> {
            final Node delta = deltaRepository.findLatestDeltaByArtifactIdAndPropertyName(artifactId, propertyName, tx);
            return deltaTransformer.map(delta.asMap());
        });
    }

    public CompletionStage<Delta> findLatestDeltaByArtifactIdAndPropertyNameAsync(final Long artifactId,
            final String propertyName,
            final Session session) {
        return session.readTransactionAsync(tx -> {
            return deltaRepository.findLatestDeltaByArtifactIdAndPropertyNameAsync(artifactId, propertyName, tx)
                    .thenApply(delta -> deltaTransformer.map(delta.asMap()));
        });
    }
}
