package neo4j.services.transformers;

import static neo4j.entities.Artifact.ID;
import static neo4j.entities.Artifact.VERSION;

import java.util.HashMap;
import java.util.Map;

import neo4j.entities.Workspace;

/**
 * Transformer for {@link Workspace}s
 *
 * @author Alexander
 *
 */
public class WorkspaceTransformer implements Transformer<Workspace>
{
    @Override
    public Workspace map(final Map<String, Object> properties) {
        final Workspace entity = new Workspace();
        entity.setId((Long) properties.get(ID));
        entity.setVersion((Long) properties.get(VERSION));
        return entity;
    }

    @Override
    public Map<String, Object> map(final Workspace entity) {
        final Map<String, Object> properties = new HashMap<>();
        properties.put(ID, entity.getId());
        properties.put(VERSION, entity.getVersion());
        return properties;
    }
}
