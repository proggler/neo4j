package neo4j.services.transformers;

import java.util.Map;

import neo4j.entities.Entity;

/**
 * A Transformer capable of transforming a map of keys and values into an entity
 * and back
 *
 * @author Alexander
 *
 * @param <E>
 */
public interface Transformer<E extends Entity>
{
    /**
     * Map a properties map of keys and values to an entity of type {@code <E>}
     *
     * @param properties
     * @return
     */
    E map(Map<String, Object> properties);

    /**
     * Map an entity of type {@code <E>} to a properties map of keys and values
     *
     * @param entity
     * @return
     */
    Map<String, Object> map(E entity);
}
