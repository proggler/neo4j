package neo4j.services.transformers;

import static neo4j.entities.Artifact.ID;
import static neo4j.entities.Artifact.VERSION;

import java.util.HashMap;
import java.util.Map;

import neo4j.entities.Artifact;

/**
 * Transformer for {@link Artifact}s
 *
 * @author Alexander
 *
 */
public class ArtifactTransformer implements Transformer<Artifact>
{
    @Override
    public Artifact map(final Map<String, Object> properties) {
        final Artifact entity = new Artifact();
        entity.setId((Long) properties.get(ID));
        entity.setVersion((Long) properties.get(VERSION));
        return entity;
    }

    @Override
    public Map<String, Object> map(final Artifact entity) {
        final Map<String, Object> properties = new HashMap<>();
        properties.put(ID, entity.getId());
        properties.put(VERSION, entity.getVersion());
        return properties;
    }
}
