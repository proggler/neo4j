package neo4j.services.transformers;

import static neo4j.entities.Delta.DATE;
import static neo4j.entities.Delta.ID;
import static neo4j.entities.Delta.NAME;
import static neo4j.entities.Delta.VALUE;
import static neo4j.entities.Delta.VERSION;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import neo4j.entities.Delta;

/**
 * Transformer for {@link Delta}s
 *
 * @author Alexander
 *
 */
public class DeltaTransformer implements Transformer<Delta>
{
    @Override
    public Delta map(final Map<String, Object> properties) {
        final Delta entity = new Delta();
        entity.setId((Long) properties.get(ID));
        entity.setVersion((Long) properties.get(VERSION));
        entity.setName((String) properties.get(NAME));
        entity.setDate(new Date((Long) properties.get(DATE)));
        entity.setValue((Serializable) properties.get(VALUE));
        return entity;
    }

    @Override
    public Map<String, Object> map(final Delta entity) {
        final Map<String, Object> properties = new HashMap<>();
        properties.put(ID, entity.getId());
        properties.put(VERSION, entity.getVersion());
        properties.put(NAME, entity.getName());
        properties.put(DATE, entity.getDate() == null ? null : entity.getDate().getTime());
        properties.put(VALUE, entity.getValue());
        return properties;
    }
}
