package neo4j;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;

import neo4j.facades.ArtifactFacade;
import neo4j.facades.ArtifactFacadeAsync;
import neo4j.facades.DeltaFacade;
import neo4j.facades.DeltaFacadeAsync;
import neo4j.facades.WorkspaceFacade;
import neo4j.facades.WorkspaceFacadeAsync;
import neo4j.repositories.nodes.ArtifactRepository;
import neo4j.repositories.nodes.DeltaRepository;
import neo4j.repositories.nodes.WorkspaceRepository;
import neo4j.repositories.relationships.Artifact2DeltaRepository;
import neo4j.repositories.relationships.Workspace2ArtifactRepository;
import neo4j.services.ArtifactService;
import neo4j.services.DeltaService;
import neo4j.services.WorkspaceService;
import neo4j.services.transformers.ArtifactTransformer;
import neo4j.services.transformers.DeltaTransformer;
import neo4j.services.transformers.WorkspaceTransformer;

public class Neo4JDatabase
{
    private final Driver driver;

    /*
     * Transformers
     */
    private final WorkspaceTransformer workspaceTransformer = new WorkspaceTransformer();
    private final ArtifactTransformer artifactTransformer = new ArtifactTransformer();
    private final DeltaTransformer deltaTransformer = new DeltaTransformer();

    /*
     * Repositories
     */
    private final WorkspaceRepository workspaceRepository = new WorkspaceRepository(workspaceTransformer);
    private final ArtifactRepository artifactRepository = new ArtifactRepository(artifactTransformer);
    private final DeltaRepository deltaRepository = new DeltaRepository(deltaTransformer);
    private final Workspace2ArtifactRepository workspace2ArtifactRepository = new Workspace2ArtifactRepository();
    private final Artifact2DeltaRepository artifact2DeltaRepository = new Artifact2DeltaRepository();

    /*
     * Services
     */
    private final WorkspaceService workspaceService = new WorkspaceService(workspaceRepository, artifactRepository,
            deltaRepository, workspace2ArtifactRepository, artifact2DeltaRepository);
    private final ArtifactService artifactService = new ArtifactService(artifactRepository, deltaRepository,
            artifact2DeltaRepository);
    private final DeltaService deltaService = new DeltaService(deltaRepository);

    /*
     * Facades
     */
    private final WorkspaceFacade workspaceFacade;
    private final ArtifactFacade artifactFacade;
    private final DeltaFacade deltaFacade;
    private final WorkspaceFacadeAsync workspaceFacadeAsync;
    private final ArtifactFacadeAsync artifactFacadeAsync;
    private final DeltaFacadeAsync deltaFacadeAsync;

    public Neo4JDatabase(final String uri, final String username, final String password) {
        this.driver = GraphDatabase.driver(uri, AuthTokens.basic(username, password));
        this.workspaceFacade = new WorkspaceFacade(driver, workspaceService);
        this.artifactFacade = new ArtifactFacade(driver, artifactService);
        this.deltaFacade = new DeltaFacade(driver, deltaService);
        this.workspaceFacadeAsync = new WorkspaceFacadeAsync(driver, workspaceService);
        this.artifactFacadeAsync = new ArtifactFacadeAsync(driver, artifactService);
        this.deltaFacadeAsync = new DeltaFacadeAsync(driver, deltaService);
        try (Session session = driver.session())
        {
            session.writeTransaction(tx -> {
                this.workspaceRepository.createIdUniqueConstraint(tx);
                this.artifactRepository.createIdUniqueConstraint(tx);
                this.deltaRepository.createIdUniqueConstraint(tx);
                this.deltaRepository.createNameIndex(tx);
                return null;
            });
        }
    }

    public WorkspaceFacade getWorkspaceFacade() {
        return workspaceFacade;
    }

    public ArtifactFacade getArtifactFacade() {
        return artifactFacade;
    }

    public DeltaFacade getDeltaFacade() {
        return deltaFacade;
    }

    public WorkspaceFacadeAsync getWorkspaceFacadeAsync() {
        return workspaceFacadeAsync;
    }

    public ArtifactFacadeAsync getArtifactFacadeAsync() {
        return artifactFacadeAsync;
    }

    public DeltaFacadeAsync getDeltaFacadeAsync() {
        return deltaFacadeAsync;
    }

    public void close() {
        driver.close();
    }
}
