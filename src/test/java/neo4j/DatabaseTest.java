package neo4j;

import org.testng.annotations.Test;

import neo4j.entities.Delta;
import neo4j.facades.DeltaFacade;

public class DatabaseTest
{
    @Test
    public void testDatabase() {
        final Neo4JDatabase db = new Neo4JDatabase("bolt://localhost:7687", "neo4j", "password");

        final DeltaFacade deltaFacade = db.getDeltaFacade();

        final Delta delta = deltaFacade.findDeltaById(532030l);
        System.out.println(delta);

        final Delta delta2 = deltaFacade.findLatestDeltaByArtifactIdAndPropertyName(63906l, "test");
        System.out.println(delta2);

        db.close();
    }
}
